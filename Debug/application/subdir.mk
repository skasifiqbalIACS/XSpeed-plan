################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../application/plotterUtility.cpp \
../application/reachabilityCaller.cpp \
../application/readCommandLine.cpp \
../application/replayPath.cpp \
../application/sfDirections.cpp \
../application/sfUtility.cpp \
../application/simulationCaller.cpp \
../application/themeSelector.cpp \
../application/userOptions.cpp \
../application/xspeed.cpp 

CPP_DEPS += \
./application/plotterUtility.d \
./application/reachabilityCaller.d \
./application/readCommandLine.d \
./application/replayPath.d \
./application/sfDirections.d \
./application/sfUtility.d \
./application/simulationCaller.d \
./application/themeSelector.d \
./application/userOptions.d \
./application/xspeed.d 

OBJS += \
./application/plotterUtility.o \
./application/reachabilityCaller.o \
./application/readCommandLine.o \
./application/replayPath.o \
./application/sfDirections.o \
./application/sfUtility.o \
./application/simulationCaller.o \
./application/themeSelector.o \
./application/userOptions.o \
./application/xspeed.o 


# Each subdirectory must supply rules for building sources it contributes
application/%.o: ../application/%.cpp application/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-application

clean-application:
	-$(RM) ./application/plotterUtility.d ./application/plotterUtility.o ./application/reachabilityCaller.d ./application/reachabilityCaller.o ./application/readCommandLine.d ./application/readCommandLine.o ./application/replayPath.d ./application/replayPath.o ./application/sfDirections.d ./application/sfDirections.o ./application/sfUtility.d ./application/sfUtility.o ./application/simulationCaller.d ./application/simulationCaller.o ./application/themeSelector.d ./application/themeSelector.o ./application/userOptions.d ./application/userOptions.o ./application/xspeed.d ./application/xspeed.o

.PHONY: clean-application

