################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ASM_SRCS := 
C++_SRCS := 
CC_SRCS := 
CPP_SRCS := 
CXX_SRCS := 
C_SRCS := 
C_UPPER_SRCS := 
OBJ_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
C++_DEPS := 
CC_DEPS := 
CPP_DEPS := 
CXX_DEPS := 
C_DEPS := 
C_UPPER_DEPS := 
EXECUTABLES := 
OBJS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
application \
benchmarks \
benchmarks/constantDynamics \
benchmarks/navigation \
benchmarks/oscillatorModel \
benchmarks/ttEthernet \
benchmarks/vehiclePlatoon \
benchmarks/waterlevelController \
build/XSpeed/application \
build/XSpeed/core/continuous/approxModel \
build/XSpeed/core/continuous/convexSet \
build/XSpeed/core/continuous/polytope \
build/XSpeed/core/discrete/discreteSet \
build/XSpeed/core/hybridAutomata \
build/XSpeed/core/math \
build/XSpeed/core/math/glpkLpSolver \
build/XSpeed/core/math/lpSolver \
build/XSpeed/core/math/nlpSolver \
build/XSpeed/core/math/numeric \
build/XSpeed/core/math/point \
build/XSpeed/core/pwl \
build/XSpeed/core/reachability \
build/XSpeed/core/symbolicStates \
build/XSpeed/counterExample \
build/XSpeed/io/flowParser \
build/XSpeed/io \
build/XSpeed/io/linExpParser \
build/XSpeed/io/resetParser \
build/XSpeed/utilities \
build/XSpeed/utilities/cpuUtilities \
build/XSpeed/utilities/directions \
build/XSpeed/utilities/memUtilities \
core/continuous/approxModel \
core/continuous/convexSet \
core/continuous/polytope \
core/discrete/discreteSet \
core/hybridAutomata \
core/math \
core/math/glpkLpSolver \
core/math/lpSolver \
core/math/nlpSolver \
core/math/numeric \
core/math/point \
core/pwl \
core/reachability \
core/symbolicStates \
counterExample \
io/flowParser \
io \
io/linExpParser \
io/resetParser \
testers/Unit_Testing_Continuous \
testers/Unit_Testing_HybridAutomata \
testers/Unit_Testing_Math \
testers/Unit_Testing_Utilities/Unit_Testing_Directions \
testers/Unit_Testing_Utilities \
testers \
utilities \
utilities/cpuUtilities \
utilities/directions \
utilities/memUtilities \

