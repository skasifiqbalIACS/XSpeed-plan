################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/math/numeric/approxComparator.cpp 

CPP_DEPS += \
./core/math/numeric/approxComparator.d 

OBJS += \
./core/math/numeric/approxComparator.o 


# Each subdirectory must supply rules for building sources it contributes
core/math/numeric/%.o: ../core/math/numeric/%.cpp core/math/numeric/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-math-2f-numeric

clean-core-2f-math-2f-numeric:
	-$(RM) ./core/math/numeric/approxComparator.d ./core/math/numeric/approxComparator.o

.PHONY: clean-core-2f-math-2f-numeric

