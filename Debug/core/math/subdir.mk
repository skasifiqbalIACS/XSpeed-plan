################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/math/2dGeometry.cpp \
../core/math/analyticOdeSol.cpp \
../core/math/matrix.cpp \
../core/math/matrixExponential.cpp \
../core/math/r8lib.cpp \
../core/math/randNumberGenerator.cpp \
../core/math/uniSphere.cpp 

CPP_DEPS += \
./core/math/2dGeometry.d \
./core/math/analyticOdeSol.d \
./core/math/matrix.d \
./core/math/matrixExponential.d \
./core/math/r8lib.d \
./core/math/randNumberGenerator.d \
./core/math/uniSphere.d 

OBJS += \
./core/math/2dGeometry.o \
./core/math/analyticOdeSol.o \
./core/math/matrix.o \
./core/math/matrixExponential.o \
./core/math/r8lib.o \
./core/math/randNumberGenerator.o \
./core/math/uniSphere.o 


# Each subdirectory must supply rules for building sources it contributes
core/math/%.o: ../core/math/%.cpp core/math/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-math

clean-core-2f-math:
	-$(RM) ./core/math/2dGeometry.d ./core/math/2dGeometry.o ./core/math/analyticOdeSol.d ./core/math/analyticOdeSol.o ./core/math/matrix.d ./core/math/matrix.o ./core/math/matrixExponential.d ./core/math/matrixExponential.o ./core/math/r8lib.d ./core/math/r8lib.o ./core/math/randNumberGenerator.d ./core/math/randNumberGenerator.o ./core/math/uniSphere.d ./core/math/uniSphere.o

.PHONY: clean-core-2f-math

