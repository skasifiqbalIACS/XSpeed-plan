################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/continuous/approxModel/approxModel.cpp \
../core/continuous/approxModel/fbInterpol.cpp 

CPP_DEPS += \
./core/continuous/approxModel/approxModel.d \
./core/continuous/approxModel/fbInterpol.d 

OBJS += \
./core/continuous/approxModel/approxModel.o \
./core/continuous/approxModel/fbInterpol.o 


# Each subdirectory must supply rules for building sources it contributes
core/continuous/approxModel/%.o: ../core/continuous/approxModel/%.cpp core/continuous/approxModel/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-continuous-2f-approxModel

clean-core-2f-continuous-2f-approxModel:
	-$(RM) ./core/continuous/approxModel/approxModel.d ./core/continuous/approxModel/approxModel.o ./core/continuous/approxModel/fbInterpol.d ./core/continuous/approxModel/fbInterpol.o

.PHONY: clean-core-2f-continuous-2f-approxModel

