################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/continuous/convexSet/createPolytopeFromConvexSet.cpp \
../core/continuous/convexSet/transMinkPoly.cpp 

CPP_DEPS += \
./core/continuous/convexSet/createPolytopeFromConvexSet.d \
./core/continuous/convexSet/transMinkPoly.d 

OBJS += \
./core/continuous/convexSet/createPolytopeFromConvexSet.o \
./core/continuous/convexSet/transMinkPoly.o 


# Each subdirectory must supply rules for building sources it contributes
core/continuous/convexSet/%.o: ../core/continuous/convexSet/%.cpp core/continuous/convexSet/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-continuous-2f-convexSet

clean-core-2f-continuous-2f-convexSet:
	-$(RM) ./core/continuous/convexSet/createPolytopeFromConvexSet.d ./core/continuous/convexSet/createPolytopeFromConvexSet.o ./core/continuous/convexSet/transMinkPoly.d ./core/continuous/convexSet/transMinkPoly.o

.PHONY: clean-core-2f-continuous-2f-convexSet

