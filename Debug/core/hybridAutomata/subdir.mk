################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/hybridAutomata/hybridAutomata.cpp \
../core/hybridAutomata/location.cpp \
../core/hybridAutomata/transition.cpp \
../core/hybridAutomata/varToIndexMap.cpp 

CPP_DEPS += \
./core/hybridAutomata/hybridAutomata.d \
./core/hybridAutomata/location.d \
./core/hybridAutomata/transition.d \
./core/hybridAutomata/varToIndexMap.d 

OBJS += \
./core/hybridAutomata/hybridAutomata.o \
./core/hybridAutomata/location.o \
./core/hybridAutomata/transition.o \
./core/hybridAutomata/varToIndexMap.o 


# Each subdirectory must supply rules for building sources it contributes
core/hybridAutomata/%.o: ../core/hybridAutomata/%.cpp core/hybridAutomata/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-hybridAutomata

clean-core-2f-hybridAutomata:
	-$(RM) ./core/hybridAutomata/hybridAutomata.d ./core/hybridAutomata/hybridAutomata.o ./core/hybridAutomata/location.d ./core/hybridAutomata/location.o ./core/hybridAutomata/transition.d ./core/hybridAutomata/transition.o ./core/hybridAutomata/varToIndexMap.d ./core/hybridAutomata/varToIndexMap.o

.PHONY: clean-core-2f-hybridAutomata

