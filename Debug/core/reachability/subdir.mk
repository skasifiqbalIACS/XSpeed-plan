################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/reachability/agjh.cpp \
../core/reachability/asyncBFS.cpp \
../core/reachability/lockAvoidanceUtility.cpp \
../core/reachability/postCParallel.cpp \
../core/reachability/postCSequential.cpp \
../core/reachability/reachability.cpp \
../core/reachability/tpbfs.cpp 

CPP_DEPS += \
./core/reachability/agjh.d \
./core/reachability/asyncBFS.d \
./core/reachability/lockAvoidanceUtility.d \
./core/reachability/postCParallel.d \
./core/reachability/postCSequential.d \
./core/reachability/reachability.d \
./core/reachability/tpbfs.d 

OBJS += \
./core/reachability/agjh.o \
./core/reachability/asyncBFS.o \
./core/reachability/lockAvoidanceUtility.o \
./core/reachability/postCParallel.o \
./core/reachability/postCSequential.o \
./core/reachability/reachability.o \
./core/reachability/tpbfs.o 


# Each subdirectory must supply rules for building sources it contributes
core/reachability/%.o: ../core/reachability/%.cpp core/reachability/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-reachability

clean-core-2f-reachability:
	-$(RM) ./core/reachability/agjh.d ./core/reachability/agjh.o ./core/reachability/asyncBFS.d ./core/reachability/asyncBFS.o ./core/reachability/lockAvoidanceUtility.d ./core/reachability/lockAvoidanceUtility.o ./core/reachability/postCParallel.d ./core/reachability/postCParallel.o ./core/reachability/postCSequential.d ./core/reachability/postCSequential.o ./core/reachability/reachability.d ./core/reachability/reachability.o ./core/reachability/tpbfs.d ./core/reachability/tpbfs.o

.PHONY: clean-core-2f-reachability

