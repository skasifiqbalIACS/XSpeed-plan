################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../core/symbolicStates/initialState.cpp \
../core/symbolicStates/symbolicStates.cpp \
../core/symbolicStates/symbolicStatesUtility.cpp 

CPP_DEPS += \
./core/symbolicStates/initialState.d \
./core/symbolicStates/symbolicStates.d \
./core/symbolicStates/symbolicStatesUtility.d 

OBJS += \
./core/symbolicStates/initialState.o \
./core/symbolicStates/symbolicStates.o \
./core/symbolicStates/symbolicStatesUtility.o 


# Each subdirectory must supply rules for building sources it contributes
core/symbolicStates/%.o: ../core/symbolicStates/%.cpp core/symbolicStates/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-core-2f-symbolicStates

clean-core-2f-symbolicStates:
	-$(RM) ./core/symbolicStates/initialState.d ./core/symbolicStates/initialState.o ./core/symbolicStates/symbolicStates.d ./core/symbolicStates/symbolicStates.o ./core/symbolicStates/symbolicStatesUtility.d ./core/symbolicStates/symbolicStatesUtility.o

.PHONY: clean-core-2f-symbolicStates

