################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../io/ioUtility.cpp \
../io/loadModel.cpp \
../io/parser.cpp 

CPP_DEPS += \
./io/ioUtility.d \
./io/loadModel.d \
./io/parser.d 

OBJS += \
./io/ioUtility.o \
./io/loadModel.o \
./io/parser.o 


# Each subdirectory must supply rules for building sources it contributes
io/%.o: ../io/%.cpp io/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-io

clean-io:
	-$(RM) ./io/ioUtility.d ./io/ioUtility.o ./io/loadModel.d ./io/loadModel.o ./io/parser.d ./io/parser.o

.PHONY: clean-io

