################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../counterExample/abstractCE.cpp \
../counterExample/abstractSymbolicState.cpp \
../counterExample/bmc.cpp \
../counterExample/concreteCE.cpp \
../counterExample/nlpFunctions.cpp \
../counterExample/simulation.cpp \
../counterExample/wofcCounterExample.cpp 

CPP_DEPS += \
./counterExample/abstractCE.d \
./counterExample/abstractSymbolicState.d \
./counterExample/bmc.d \
./counterExample/concreteCE.d \
./counterExample/nlpFunctions.d \
./counterExample/simulation.d \
./counterExample/wofcCounterExample.d 

OBJS += \
./counterExample/abstractCE.o \
./counterExample/abstractSymbolicState.o \
./counterExample/bmc.o \
./counterExample/concreteCE.o \
./counterExample/nlpFunctions.o \
./counterExample/simulation.o \
./counterExample/wofcCounterExample.o 


# Each subdirectory must supply rules for building sources it contributes
counterExample/%.o: ../counterExample/%.cpp counterExample/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-counterExample

clean-counterExample:
	-$(RM) ./counterExample/abstractCE.d ./counterExample/abstractCE.o ./counterExample/abstractSymbolicState.d ./counterExample/abstractSymbolicState.o ./counterExample/bmc.d ./counterExample/bmc.o ./counterExample/concreteCE.d ./counterExample/concreteCE.o ./counterExample/nlpFunctions.d ./counterExample/nlpFunctions.o ./counterExample/simulation.d ./counterExample/simulation.o ./counterExample/wofcCounterExample.d ./counterExample/wofcCounterExample.o

.PHONY: clean-counterExample

