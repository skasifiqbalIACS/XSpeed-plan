################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../benchmarks/constantDynamics/constantMotion.cpp 

CPP_DEPS += \
./benchmarks/constantDynamics/constantMotion.d 

OBJS += \
./benchmarks/constantDynamics/constantMotion.o 


# Each subdirectory must supply rules for building sources it contributes
benchmarks/constantDynamics/%.o: ../benchmarks/constantDynamics/%.cpp benchmarks/constantDynamics/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-benchmarks-2f-constantDynamics

clean-benchmarks-2f-constantDynamics:
	-$(RM) ./benchmarks/constantDynamics/constantMotion.d ./benchmarks/constantDynamics/constantMotion.o

.PHONY: clean-benchmarks-2f-constantDynamics

