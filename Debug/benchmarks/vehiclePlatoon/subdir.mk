################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../benchmarks/vehiclePlatoon/vehicle10_platoon.cpp \
../benchmarks/vehiclePlatoon/vehicle5_platoon.cpp 

CPP_DEPS += \
./benchmarks/vehiclePlatoon/vehicle10_platoon.d \
./benchmarks/vehiclePlatoon/vehicle5_platoon.d 

OBJS += \
./benchmarks/vehiclePlatoon/vehicle10_platoon.o \
./benchmarks/vehiclePlatoon/vehicle5_platoon.o 


# Each subdirectory must supply rules for building sources it contributes
benchmarks/vehiclePlatoon/%.o: ../benchmarks/vehiclePlatoon/%.cpp benchmarks/vehiclePlatoon/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-benchmarks-2f-vehiclePlatoon

clean-benchmarks-2f-vehiclePlatoon:
	-$(RM) ./benchmarks/vehiclePlatoon/vehicle10_platoon.d ./benchmarks/vehiclePlatoon/vehicle10_platoon.o ./benchmarks/vehiclePlatoon/vehicle5_platoon.d ./benchmarks/vehiclePlatoon/vehicle5_platoon.o

.PHONY: clean-benchmarks-2f-vehiclePlatoon

