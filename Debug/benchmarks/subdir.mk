################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../benchmarks/bouncingBall.cpp \
../benchmarks/fiveDimSys.cpp \
../benchmarks/platoon.cpp \
../benchmarks/spacecraft.cpp \
../benchmarks/timedBouncingBall.cpp 

CPP_DEPS += \
./benchmarks/bouncingBall.d \
./benchmarks/fiveDimSys.d \
./benchmarks/platoon.d \
./benchmarks/spacecraft.d \
./benchmarks/timedBouncingBall.d 

OBJS += \
./benchmarks/bouncingBall.o \
./benchmarks/fiveDimSys.o \
./benchmarks/platoon.o \
./benchmarks/spacecraft.o \
./benchmarks/timedBouncingBall.o 


# Each subdirectory must supply rules for building sources it contributes
benchmarks/%.o: ../benchmarks/%.cpp benchmarks/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-benchmarks

clean-benchmarks:
	-$(RM) ./benchmarks/bouncingBall.d ./benchmarks/bouncingBall.o ./benchmarks/fiveDimSys.d ./benchmarks/fiveDimSys.o ./benchmarks/platoon.d ./benchmarks/platoon.o ./benchmarks/spacecraft.d ./benchmarks/spacecraft.o ./benchmarks/timedBouncingBall.d ./benchmarks/timedBouncingBall.o

.PHONY: clean-benchmarks

