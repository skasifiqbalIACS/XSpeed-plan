################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../benchmarks/oscillatorModel/Oscillator.cpp \
../benchmarks/oscillatorModel/Oscillator_timed.cpp \
../benchmarks/oscillatorModel/f_osc_32.cpp \
../benchmarks/oscillatorModel/f_osc_32_timed.cpp \
../benchmarks/oscillatorModel/f_osc_8.cpp \
../benchmarks/oscillatorModel/f_osc_8_timed.cpp 

CPP_DEPS += \
./benchmarks/oscillatorModel/Oscillator.d \
./benchmarks/oscillatorModel/Oscillator_timed.d \
./benchmarks/oscillatorModel/f_osc_32.d \
./benchmarks/oscillatorModel/f_osc_32_timed.d \
./benchmarks/oscillatorModel/f_osc_8.d \
./benchmarks/oscillatorModel/f_osc_8_timed.d 

OBJS += \
./benchmarks/oscillatorModel/Oscillator.o \
./benchmarks/oscillatorModel/Oscillator_timed.o \
./benchmarks/oscillatorModel/f_osc_32.o \
./benchmarks/oscillatorModel/f_osc_32_timed.o \
./benchmarks/oscillatorModel/f_osc_8.o \
./benchmarks/oscillatorModel/f_osc_8_timed.o 


# Each subdirectory must supply rules for building sources it contributes
benchmarks/oscillatorModel/%.o: ../benchmarks/oscillatorModel/%.cpp benchmarks/oscillatorModel/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-benchmarks-2f-oscillatorModel

clean-benchmarks-2f-oscillatorModel:
	-$(RM) ./benchmarks/oscillatorModel/Oscillator.d ./benchmarks/oscillatorModel/Oscillator.o ./benchmarks/oscillatorModel/Oscillator_timed.d ./benchmarks/oscillatorModel/Oscillator_timed.o ./benchmarks/oscillatorModel/f_osc_32.d ./benchmarks/oscillatorModel/f_osc_32.o ./benchmarks/oscillatorModel/f_osc_32_timed.d ./benchmarks/oscillatorModel/f_osc_32_timed.o ./benchmarks/oscillatorModel/f_osc_8.d ./benchmarks/oscillatorModel/f_osc_8.o ./benchmarks/oscillatorModel/f_osc_8_timed.d ./benchmarks/oscillatorModel/f_osc_8_timed.o

.PHONY: clean-benchmarks-2f-oscillatorModel

