################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../benchmarks/navigation/nav04Timed.cpp \
../benchmarks/navigation/nav22Timed.cpp \
../benchmarks/navigation/nav25Timed.cpp \
../benchmarks/navigation/nav30Timed.cpp \
../benchmarks/navigation/navT02Timed.cpp \
../benchmarks/navigation/navT04Timed.cpp \
../benchmarks/navigation/navigation5by5.cpp 

CPP_DEPS += \
./benchmarks/navigation/nav04Timed.d \
./benchmarks/navigation/nav22Timed.d \
./benchmarks/navigation/nav25Timed.d \
./benchmarks/navigation/nav30Timed.d \
./benchmarks/navigation/navT02Timed.d \
./benchmarks/navigation/navT04Timed.d \
./benchmarks/navigation/navigation5by5.d 

OBJS += \
./benchmarks/navigation/nav04Timed.o \
./benchmarks/navigation/nav22Timed.o \
./benchmarks/navigation/nav25Timed.o \
./benchmarks/navigation/nav30Timed.o \
./benchmarks/navigation/navT02Timed.o \
./benchmarks/navigation/navT04Timed.o \
./benchmarks/navigation/navigation5by5.o 


# Each subdirectory must supply rules for building sources it contributes
benchmarks/navigation/%.o: ../benchmarks/navigation/%.cpp benchmarks/navigation/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-benchmarks-2f-navigation

clean-benchmarks-2f-navigation:
	-$(RM) ./benchmarks/navigation/nav04Timed.d ./benchmarks/navigation/nav04Timed.o ./benchmarks/navigation/nav22Timed.d ./benchmarks/navigation/nav22Timed.o ./benchmarks/navigation/nav25Timed.d ./benchmarks/navigation/nav25Timed.o ./benchmarks/navigation/nav30Timed.d ./benchmarks/navigation/nav30Timed.o ./benchmarks/navigation/navT02Timed.d ./benchmarks/navigation/navT02Timed.o ./benchmarks/navigation/navT04Timed.d ./benchmarks/navigation/navT04Timed.o ./benchmarks/navigation/navigation5by5.d ./benchmarks/navigation/navigation5by5.o

.PHONY: clean-benchmarks-2f-navigation

