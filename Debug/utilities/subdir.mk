################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../utilities/computeInitialPolytope.cpp \
../utilities/directionalApproxError.cpp \
../utilities/dumpAbstractCEList.cpp \
../utilities/flowCostEstimate.cpp \
../utilities/flowpipeCluster.cpp \
../utilities/gradient.cpp \
../utilities/hausdorffDistance.cpp \
../utilities/invariantBoundaryCheck.cpp \
../utilities/postAssignment.cpp \
../utilities/samples.cpp \
../utilities/statistics.cpp \
../utilities/stringParser.cpp \
../utilities/templatePolyhedra.cpp \
../utilities/testPolytopePlotting.cpp \
../utilities/vectorOperations.cpp 

CPP_DEPS += \
./utilities/computeInitialPolytope.d \
./utilities/directionalApproxError.d \
./utilities/dumpAbstractCEList.d \
./utilities/flowCostEstimate.d \
./utilities/flowpipeCluster.d \
./utilities/gradient.d \
./utilities/hausdorffDistance.d \
./utilities/invariantBoundaryCheck.d \
./utilities/postAssignment.d \
./utilities/samples.d \
./utilities/statistics.d \
./utilities/stringParser.d \
./utilities/templatePolyhedra.d \
./utilities/testPolytopePlotting.d \
./utilities/vectorOperations.d 

OBJS += \
./utilities/computeInitialPolytope.o \
./utilities/directionalApproxError.o \
./utilities/dumpAbstractCEList.o \
./utilities/flowCostEstimate.o \
./utilities/flowpipeCluster.o \
./utilities/gradient.o \
./utilities/hausdorffDistance.o \
./utilities/invariantBoundaryCheck.o \
./utilities/postAssignment.o \
./utilities/samples.o \
./utilities/statistics.o \
./utilities/stringParser.o \
./utilities/templatePolyhedra.o \
./utilities/testPolytopePlotting.o \
./utilities/vectorOperations.o 


# Each subdirectory must supply rules for building sources it contributes
utilities/%.o: ../utilities/%.cpp utilities/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-utilities

clean-utilities:
	-$(RM) ./utilities/computeInitialPolytope.d ./utilities/computeInitialPolytope.o ./utilities/directionalApproxError.d ./utilities/directionalApproxError.o ./utilities/dumpAbstractCEList.d ./utilities/dumpAbstractCEList.o ./utilities/flowCostEstimate.d ./utilities/flowCostEstimate.o ./utilities/flowpipeCluster.d ./utilities/flowpipeCluster.o ./utilities/gradient.d ./utilities/gradient.o ./utilities/hausdorffDistance.d ./utilities/hausdorffDistance.o ./utilities/invariantBoundaryCheck.d ./utilities/invariantBoundaryCheck.o ./utilities/postAssignment.d ./utilities/postAssignment.o ./utilities/samples.d ./utilities/samples.o ./utilities/statistics.d ./utilities/statistics.o ./utilities/stringParser.d ./utilities/stringParser.o ./utilities/templatePolyhedra.d ./utilities/templatePolyhedra.o ./utilities/testPolytopePlotting.d ./utilities/testPolytopePlotting.o ./utilities/vectorOperations.d ./utilities/vectorOperations.o

.PHONY: clean-utilities

