################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../testers/Unit_Testing_Utilities/test_directions.cpp \
../testers/Unit_Testing_Utilities/test_postAssignment.cpp \
../testers/Unit_Testing_Utilities/test_templatePolyhedra.cpp 

CPP_DEPS += \
./testers/Unit_Testing_Utilities/test_directions.d \
./testers/Unit_Testing_Utilities/test_postAssignment.d \
./testers/Unit_Testing_Utilities/test_templatePolyhedra.d 

OBJS += \
./testers/Unit_Testing_Utilities/test_directions.o \
./testers/Unit_Testing_Utilities/test_postAssignment.o \
./testers/Unit_Testing_Utilities/test_templatePolyhedra.o 


# Each subdirectory must supply rules for building sources it contributes
testers/Unit_Testing_Utilities/%.o: ../testers/Unit_Testing_Utilities/%.cpp testers/Unit_Testing_Utilities/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-testers-2f-Unit_Testing_Utilities

clean-testers-2f-Unit_Testing_Utilities:
	-$(RM) ./testers/Unit_Testing_Utilities/test_directions.d ./testers/Unit_Testing_Utilities/test_directions.o ./testers/Unit_Testing_Utilities/test_postAssignment.d ./testers/Unit_Testing_Utilities/test_postAssignment.o ./testers/Unit_Testing_Utilities/test_templatePolyhedra.d ./testers/Unit_Testing_Utilities/test_templatePolyhedra.o

.PHONY: clean-testers-2f-Unit_Testing_Utilities

