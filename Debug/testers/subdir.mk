################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../testers/test_runner.cpp 

CPP_DEPS += \
./testers/test_runner.d 

OBJS += \
./testers/test_runner.o 


# Each subdirectory must supply rules for building sources it contributes
testers/%.o: ../testers/%.cpp testers/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-testers

clean-testers:
	-$(RM) ./testers/test_runner.d ./testers/test_runner.o

.PHONY: clean-testers

