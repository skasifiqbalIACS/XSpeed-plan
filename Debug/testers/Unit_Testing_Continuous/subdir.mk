################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../testers/Unit_Testing_Continuous/test_lp_solver.cpp \
../testers/Unit_Testing_Continuous/test_matrix.cpp \
../testers/Unit_Testing_Continuous/test_polytope.cpp \
../testers/Unit_Testing_Continuous/test_polytope_enumerate_vertices.cpp 

CPP_DEPS += \
./testers/Unit_Testing_Continuous/test_lp_solver.d \
./testers/Unit_Testing_Continuous/test_matrix.d \
./testers/Unit_Testing_Continuous/test_polytope.d \
./testers/Unit_Testing_Continuous/test_polytope_enumerate_vertices.d 

OBJS += \
./testers/Unit_Testing_Continuous/test_lp_solver.o \
./testers/Unit_Testing_Continuous/test_matrix.o \
./testers/Unit_Testing_Continuous/test_polytope.o \
./testers/Unit_Testing_Continuous/test_polytope_enumerate_vertices.o 


# Each subdirectory must supply rules for building sources it contributes
testers/Unit_Testing_Continuous/%.o: ../testers/Unit_Testing_Continuous/%.cpp testers/Unit_Testing_Continuous/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-testers-2f-Unit_Testing_Continuous

clean-testers-2f-Unit_Testing_Continuous:
	-$(RM) ./testers/Unit_Testing_Continuous/test_lp_solver.d ./testers/Unit_Testing_Continuous/test_lp_solver.o ./testers/Unit_Testing_Continuous/test_matrix.d ./testers/Unit_Testing_Continuous/test_matrix.o ./testers/Unit_Testing_Continuous/test_polytope.d ./testers/Unit_Testing_Continuous/test_polytope.o ./testers/Unit_Testing_Continuous/test_polytope_enumerate_vertices.d ./testers/Unit_Testing_Continuous/test_polytope_enumerate_vertices.o

.PHONY: clean-testers-2f-Unit_Testing_Continuous

