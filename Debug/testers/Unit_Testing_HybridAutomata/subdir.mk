################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../testers/Unit_Testing_HybridAutomata/test_hybridAutomata.cpp \
../testers/Unit_Testing_HybridAutomata/test_location.cpp \
../testers/Unit_Testing_HybridAutomata/test_transition.cpp 

CPP_DEPS += \
./testers/Unit_Testing_HybridAutomata/test_hybridAutomata.d \
./testers/Unit_Testing_HybridAutomata/test_location.d \
./testers/Unit_Testing_HybridAutomata/test_transition.d 

OBJS += \
./testers/Unit_Testing_HybridAutomata/test_hybridAutomata.o \
./testers/Unit_Testing_HybridAutomata/test_location.o \
./testers/Unit_Testing_HybridAutomata/test_transition.o 


# Each subdirectory must supply rules for building sources it contributes
testers/Unit_Testing_HybridAutomata/%.o: ../testers/Unit_Testing_HybridAutomata/%.cpp testers/Unit_Testing_HybridAutomata/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-testers-2f-Unit_Testing_HybridAutomata

clean-testers-2f-Unit_Testing_HybridAutomata:
	-$(RM) ./testers/Unit_Testing_HybridAutomata/test_hybridAutomata.d ./testers/Unit_Testing_HybridAutomata/test_hybridAutomata.o ./testers/Unit_Testing_HybridAutomata/test_location.d ./testers/Unit_Testing_HybridAutomata/test_location.o ./testers/Unit_Testing_HybridAutomata/test_transition.d ./testers/Unit_Testing_HybridAutomata/test_transition.o

.PHONY: clean-testers-2f-Unit_Testing_HybridAutomata

