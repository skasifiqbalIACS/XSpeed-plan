################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../testers/Unit_Testing_Math/test_2d_geometry.cpp \
../testers/Unit_Testing_Math/test_NLPsolver.cpp \
../testers/Unit_Testing_Math/test_ProjectionDist.cpp \
../testers/Unit_Testing_Math/test_glpk.cpp \
../testers/Unit_Testing_Math/test_hausdorffDist.cpp \
../testers/Unit_Testing_Math/test_math_matrix.cpp \
../testers/Unit_Testing_Math/test_nlopt.cpp \
../testers/Unit_Testing_Math/test_ode_solution.cpp \
../testers/Unit_Testing_Math/test_randNumberGenerator.cpp \
../testers/Unit_Testing_Math/test_simplex.cpp \
../testers/Unit_Testing_Math/test_simulation.cpp 

CPP_DEPS += \
./testers/Unit_Testing_Math/test_2d_geometry.d \
./testers/Unit_Testing_Math/test_NLPsolver.d \
./testers/Unit_Testing_Math/test_ProjectionDist.d \
./testers/Unit_Testing_Math/test_glpk.d \
./testers/Unit_Testing_Math/test_hausdorffDist.d \
./testers/Unit_Testing_Math/test_math_matrix.d \
./testers/Unit_Testing_Math/test_nlopt.d \
./testers/Unit_Testing_Math/test_ode_solution.d \
./testers/Unit_Testing_Math/test_randNumberGenerator.d \
./testers/Unit_Testing_Math/test_simplex.d \
./testers/Unit_Testing_Math/test_simulation.d 

OBJS += \
./testers/Unit_Testing_Math/test_2d_geometry.o \
./testers/Unit_Testing_Math/test_NLPsolver.o \
./testers/Unit_Testing_Math/test_ProjectionDist.o \
./testers/Unit_Testing_Math/test_glpk.o \
./testers/Unit_Testing_Math/test_hausdorffDist.o \
./testers/Unit_Testing_Math/test_math_matrix.o \
./testers/Unit_Testing_Math/test_nlopt.o \
./testers/Unit_Testing_Math/test_ode_solution.o \
./testers/Unit_Testing_Math/test_randNumberGenerator.o \
./testers/Unit_Testing_Math/test_simplex.o \
./testers/Unit_Testing_Math/test_simulation.o 


# Each subdirectory must supply rules for building sources it contributes
testers/Unit_Testing_Math/%.o: ../testers/Unit_Testing_Math/%.cpp testers/Unit_Testing_Math/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include -I"/home/asif/eclipse-workspace/XSpeed-plan" -I/home/asif/eclipse-workspace/XSpeed-plan -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-testers-2f-Unit_Testing_Math

clean-testers-2f-Unit_Testing_Math:
	-$(RM) ./testers/Unit_Testing_Math/test_2d_geometry.d ./testers/Unit_Testing_Math/test_2d_geometry.o ./testers/Unit_Testing_Math/test_NLPsolver.d ./testers/Unit_Testing_Math/test_NLPsolver.o ./testers/Unit_Testing_Math/test_ProjectionDist.d ./testers/Unit_Testing_Math/test_ProjectionDist.o ./testers/Unit_Testing_Math/test_glpk.d ./testers/Unit_Testing_Math/test_glpk.o ./testers/Unit_Testing_Math/test_hausdorffDist.d ./testers/Unit_Testing_Math/test_hausdorffDist.o ./testers/Unit_Testing_Math/test_math_matrix.d ./testers/Unit_Testing_Math/test_math_matrix.o ./testers/Unit_Testing_Math/test_nlopt.d ./testers/Unit_Testing_Math/test_nlopt.o ./testers/Unit_Testing_Math/test_ode_solution.d ./testers/Unit_Testing_Math/test_ode_solution.o ./testers/Unit_Testing_Math/test_randNumberGenerator.d ./testers/Unit_Testing_Math/test_randNumberGenerator.o ./testers/Unit_Testing_Math/test_simplex.d ./testers/Unit_Testing_Math/test_simplex.o ./testers/Unit_Testing_Math/test_simulation.d ./testers/Unit_Testing_Math/test_simulation.o

.PHONY: clean-testers-2f-Unit_Testing_Math

