/*
 * themeSelector.cpp
 *
 *  Created on: 12-Aug-2020
 *      Author: rajarshi
 */

#include <application/themeSelector.h>
#include <fstream>
#include <chrono>


themeSelector::themeSelector() {
	// TODO Auto-generated constructor stub
}
void themeSelector::setUserOps(userOptions& op) {
	this->userOps = op;
}

void themeSelector::setInit(std::list<initial_state::ptr>& init){
	this->init = init;
}

void themeSelector::setReachParams(ReachabilityParameters& params) {
	this->reach_params= params;
}

void themeSelector::setForbidden(forbidden_states& forbidden) {
	this->forbidden = forbidden;
}

userOptions& themeSelector::getUserOps(){
	return this->userOps;
}
hybrid_automata::ptr themeSelector::getHaInstance() {
	return ha_ptr;
}
std::list<initial_state::ptr>& themeSelector::getInit() {
	return this->init;
}
ReachabilityParameters& themeSelector::getReachParams(){
	return this->reach_params;
}
forbidden_states& themeSelector::getForbidden(){
	return this->forbidden;
}

void themeSelector::selectReach()
{
	std::list<symbolic_states::ptr> symbolic_states;
	std::list<abstractCE::ptr> ce_candidates; //object of class counter_example

	boost::timer::cpu_timer timer, plottime;
	unsigned int number_of_times = 1;	//For reporting average time
	unsigned int lp_solver_type = 1;	// choose the glpk solver
	timer.start();
	init_cpu_usage(); //initializing the CPU Usage utility to start recording usages
	for (unsigned int i = 1; i <= number_of_times; i++) { //Running in a loop of number_of_times to compute the average result
		// Calls the reachability computation routine.
		reachabilityCaller(*ha_ptr, init, reach_params, userOps, lp_solver_type, forbidden, symbolic_states, ce_candidates);
	}
	timer.stop();
	double cpu_usage = getCurrent_ProcessCPU_usage();
	long mem_usage = getCurrentProcess_PhysicalMemoryUsed();
	print_statistics(timer,cpu_usage,mem_usage, number_of_times, "Reachability Analysis and CE Search");


	// Choosing from the output format and showing results

	if(ha_ptr->ymap_size()!=0){
		//transform the sfm to output directions before plotting
		std::list<symbolic_states::ptr>::iterator it =  symbolic_states.begin();
		for(;it!=symbolic_states.end(); it++){
			symbolic_states::ptr symbStatePtr = *it;
			transformTemplatePoly(*ha_ptr, symbStatePtr->getContinuousSetptr());
		}
	}

	plottime.start();
	show(symbolic_states, userOps);
	plottime.stop();
//	print_statistics(plottime,"Plotting");

	// printing the first initial polytope in the init_poly file
	polytope::const_ptr init_poly = (*init.begin())->getInitialSet();
	init_poly->print2file("./init_poly",userOps.get_first_plot_dimension(),userOps.get_second_plot_dimension());


}
void themeSelector::selectSim(){
	std::cout << "Running simulation engine ... \n";
	if (forbidden.size() > 0)
		simulationCaller(*ha_ptr, init, reach_params, forbidden[0], userOps);
	else{
		// create an empty forbidden region
		std::pair<int, polytope::ptr> forbidden_s;
		forbidden_s.first = -10; // implies no location
		forbidden_s.second = polytope::ptr(new polytope(true)); // empty polytope
		simulationCaller(*ha_ptr, init, reach_params, forbidden_s, userOps);
	}
}

void themeSelector::selectFal(){
	//todo: call the path-oriented falsification routine.
	boost::timer::cpu_timer timer;
	unsigned int number_of_times = 1;

	bmc bmc_fal(ha_ptr, init, forbidden, reach_params, userOps);

	timer.start();
	init_cpu_usage();

	unsigned int safe = bmc_fal.safe();
	timer.stop();
	double cpu_usage = getCurrent_ProcessCPU_usage();
	long mem_usage = getCurrentProcess_PhysicalMemoryUsed();
	print_statistics(timer,cpu_usage,mem_usage, number_of_times, "Bounded Model Checking");
	
	// printing the first initial polytope in the init_poly file
	polytope::const_ptr init_poly = (*init.begin())->getInitialSet();
	init_poly->print2file("./init_poly",userOps.get_first_plot_dimension(),userOps.get_second_plot_dimension());


	if(safe == 1)
		std::cout << "BMC: The model is SAFE" << std::endl;
	else if(safe == 0)
		std::cout << "BMC: The model is UNSAFE" << std::endl;
	else
		std::cout<<"BMC: The safety of the model is UNKNOWN"<<std::endl;
}

void themeSelector::selectReplay(){
	//todo: call the path based replay routine
	//replayPath rply(ha_ptr, ha_ptr2, init, forbidden, userOps);
	replayPath rply(ha_ptr, init, forbidden, userOps);
//	replayPath rply(ha_ptr, ha_ptr1, init, forbidden, userOps);

//	std::list<transition::ptr> Edges; //all return edges of d_Replay().
//	std::list<structuralPath::ptr> infeasiblePaths;	//all infeasible paths of cH_Replay
//	std::list<std::pair<vector<int>,vector<int> > > IIS_pathSegments; //all IIS path segments of cA_Replay
//	int feasible_paths_in_human_model = 0;
//	int paths_replayed_in_agent_model =0;


	/****** print locations and transitions of human model *********/
	std::map<int, location::ptr> allLoc1 = ha_ptr->getAllLocations();
	std::cout<<"Locations and transitions in the human model:"<<endl;
	for(auto it = allLoc1.begin(); it != allLoc1.end(); it++){
		int loc_id = it->first;
		std::cout<<"loc id: "<<loc_id<<std::endl;
		const std::list<transition::ptr>& out_trans = it->second->getOutGoingTransitions();
		std::list<transition::ptr>::const_iterator itr;
			for (itr=out_trans.begin(); itr !=out_trans.end(); itr++){
				int transID = (*itr)->getTransitionId();
				std::cout<<"trans id: "<<transID<<std::endl;
			}
	}


	//rply.modifyAgentHA(2, 2, 3);


	/****** retrieve structural paths in the human model ******/
	std::list<structuralPath::ptr> paths;
	location::ptr source_ptr = ha_ptr->getInitialLocation();
	int initial_id = source_ptr->getLocId();
	int forbid_id = forbidden[0].first;
	unsigned int depth = userOps.get_bfs_level();
	std::cout<<"initial loc: "<<initial_id<<"  goal loc: "<< forbid_id<<"  path depth: "<< depth<<endl;
	//std::cout<<"The number of paths are: "<<endl;
	paths = rply.getStructuralPath(ha_ptr, initial_id, forbid_id, depth);
	//paths = rply.getStructuralPath(2, 4, 3);
	std::cout<<"Number of paths in the human model: "<<paths.size()<<endl;

	/****** print all structural paths of the human model (locations only) ******/
	std::list<structuralPath::ptr>::iterator pitr;
	int pathNum = 0;  //for numbering all paths
	/**** for storing paths ***/
	fstream fileout;
	fileout.open("path_of_human2.txt", ios::out);
	/******/
		for (pitr=paths.begin(); pitr !=paths.end(); pitr++){

			pathNum++;
			std::cout<<pathNum<<": ";
			fileout<<"Path "<<pathNum<<": ";
			std::list<location::const_ptr> path_locations = (*pitr)->get_path_locations(); // the list containing the locations of the path, in order
			for (auto it = path_locations.begin(); it != path_locations.end(); it++){
				std::cout<<(*it)->getLocId()<<"  ";
				fileout<<(*it)->getLocId()<<"  ";
			}
			std::cout<<""<<endl;
			fileout<<""<<endl;
			std::list<transition::ptr> path_transitions = (*pitr)->get_path_transitions(); // the list containing the transitions of the path, in order
            for (auto it = path_transitions.begin(); it != path_transitions.end(); it++){
            	std::cout<<(*it)->getTransitionId()<<" ";
            }
			std::cout<<""<<endl;
		}
		fileout.close();


	/****** print locations and transitions of the agent model ******/
//	std::map<int, location::ptr> allLoc2 = ha_ptr1->getAllLocations();
//	std::cout<<"Locations and transitions in the agent model:"<<endl;
//	for(auto it = allLoc2.begin(); it != allLoc2.end(); it++){
//		int loc_id = it->first;
//		std::cout<<"loc id: "<<loc_id<<std::endl;
//		const std::list<transition::ptr>& out_trans = it->second->getOutGoingTransitions();
//		std::list<transition::ptr>::const_iterator itr;
//			for (itr=out_trans.begin(); itr !=out_trans.end(); itr++){
//				int transID = (*itr)->getTransitionId();
//				std::cout<<"trans id: "<<transID<<std::endl;
//			}
//	}

	/****** retrieve structural paths of the agent model ******/
//	std::list<structuralPath::ptr> paths1;
//	location::ptr source_ptr1 = ha_ptr1->getInitialLocation();
//	int initial_id1 = source_ptr1->getLocId();
//	int forbid_id1 = forbidden[0].first;
//	unsigned int depth1 = userOps.get_bfs_level();
//	//std::cout<<initial_id1<<"\t"<< forbid_id1<<"\t"<< depth1<<endl;
//	//std::cout<<"The number of paths are: "<<endl;
//	paths1 = rply.getStructuralPath(ha_ptr1, initial_id1, forbid_id1, depth1);
//	//paths = rply.getStructuralPath(2, 4, 3);
//	std::cout<<"Number of paths in the agent model: "<<paths1.size()<<endl;


	/****** Calling d_Replay() (discrete replay) to replay all paths (of human model) ******/
//	int pathNo = 0; //for numbering the path
//	for (pitr=paths.begin(); pitr !=paths.end(); pitr++){
//		pathNo++;
//
//		std::list<location::const_ptr> path_locations = (*pitr)->get_path_locations(); // the list containing the locations of the path, in order
//		std::list<location::const_ptr>::iterator lptr;
//		std::list<transition::ptr> path_transitions = (*pitr)->get_path_transitions(); // the list containing the transitions of the path, in order
//		std::list<transition::ptr>::iterator tptr;
//
//		std::cout<<"Path No "<<pathNo<<": The path being replayed: ";
//		for (auto it = path_locations.begin(); it != path_locations.end(); it++){
//			std::cout<<(*it)->getLocId()<<" ";
//		}
//		std::cout<<""<<endl;
//
//		/*** check path in infeasiblePaths (to do) ***/
//		std::list<structuralPath::ptr>::iterator infeasiblePath_itr;
//		bool path_is_infeasible = false;
//		for (infeasiblePath_itr = infeasiblePaths.begin(); infeasiblePath_itr != infeasiblePaths.end(); infeasiblePath_itr++){
//			std::list<location::const_ptr> i_path_locations = (*infeasiblePath_itr)->get_path_locations();
//			std::list<location::const_ptr>::iterator i_lptr;
//			std::list<transition::ptr> i_path_transitions = (*infeasiblePath_itr)->get_path_transitions();
//			std::list<transition::ptr>::iterator i_tptr;
//			for (int i = 0; i < i_path_locations.size(); i++){
//				lptr = path_locations.begin();
//				i_lptr = i_path_locations.begin();
//				tptr = path_transitions.begin();
//				i_tptr = i_path_transitions.begin();
//				if (i != (i_path_locations.size() - 1)){
//					std::advance(lptr, i);
//					std::advance(i_lptr, i);
//					int loc_id = (*lptr)->getLocId();
//					int i_loc_id = (*i_lptr)->getLocId();
//					if (i_loc_id == loc_id){
//						std::advance(tptr, i);
//						int trans_id = (*tptr)->getTransitionId();
//						int i_trans_id = (*i_tptr)->getTransitionId();
//						if (i_trans_id == trans_id){
//							continue;
//						}
//						else break;
//					}
//					else break;
//				}
//				else {
//					std::advance(lptr, i);
//					int loc_id = (*lptr)->getLocId();
//					std::advance(i_lptr, i);
//					int i_loc_id = (*i_lptr)->getLocId();
//					if (i_loc_id == loc_id){
//						path_is_infeasible = true;
//						std::cout<<"Path contains infeasible path segments."<<endl;
//						break;
//					}
//				}
//			}
//			if (path_is_infeasible == true) {
//				break;
//			}
//		}
//		if (path_is_infeasible == true) {
//			std::cout<<"Path No: "<<pathNo<<" --The current path contains Infeasible Path Segment : skipping the path."<<endl;
//			infeasiblePaths.push_back(*pitr); //pushing the path in the infeasible path set
//			continue;
//		}
//
//		/*** Infeasible paths END ***/
//
//		/*** check path for IIS_pathSegments #BAD_PREFIX ***/
//		std::list<std::pair<vector<int>,vector<int>>>::iterator IIS_itr;
//		//bool bad_prefix_found = false;
//		bool IIS_path_segment_found = false;
//		for (IIS_itr = IIS_pathSegments.begin(); IIS_itr != IIS_pathSegments.end(); IIS_itr++){
//			vector<int> loc_ids = IIS_itr->first;
//			vector<int> trans_ids = IIS_itr->second;
//			int i = 0;
//			int j = 0;
//			while (j < loc_ids.size()){
//				lptr = path_locations.begin();
//				tptr = path_transitions.begin();
//				std::advance(lptr, i);
//				int loc_id = (*lptr)->getLocId();
//				//std::cout<<"loc id: "<<loc_id<<" IIS loc id: "<<loc_ids[j]<<endl;
//				if ((loc_ids[j] == loc_id) and (j < trans_ids.size())){
//					std::advance(tptr, i);
//					int trans_id = (*tptr)->getTransitionId();
//					if (trans_ids[j] == trans_id){
//						i++;
//						j++;
//					/*	if (j == loc_ids.size()){
//							std::cout<<"test4"<<endl;
//							IIS_path_segment_found = true;
//							std::cout<<"Path contains IIS path segment"<<endl;
//							break;
//						} */
//					}
//					else {
//						i++;
//						j = 0;
//						if (i > path_locations.size() - loc_ids.size()){
//							break;
//						}
//					}
//				}
//				else if ((loc_ids[j] == loc_id) and (j == trans_ids.size())) {
//					IIS_path_segment_found = true;
//					std::cout<<"Path contains IIS path segment"<<endl;
//					break;
//				}
//				else if (loc_ids[j] != loc_id){
//					i++;
//					j = 0;
//					if (i > path_locations.size() - loc_ids.size()){
//						break;
//					}
//				}
//			}
//
//
///*			for (int i = 0; i < loc_ids.size(); i++){
//				lptr = path_locations.begin();
//				tptr = path_transitions.begin();
//				if (i != (loc_ids.size() - 1)){
//					std::advance(lptr, i);
//					int loc_id = (*lptr)->getLocId();
//					if (loc_ids[i] == loc_id){
//						std::advance(tptr, i);
//						int trans_id = (*tptr)->getTransitionId();
//						if (trans_ids[i] == trans_id){
//							continue;
//						}
//						else break;
//					}
//					else break;
//				}
//				else {
//					std::advance(lptr, i);
//					int loc_id = (*lptr)->getLocId();
//					if (loc_ids[i] == loc_id){
//						bad_prefix_found = true;
//						std::cout<<"Path contains BAD_PREFIX"<<endl;
//						break;
//					}
//				}
//
//			} */
//
//			if (IIS_path_segment_found == true) {
//				break;
//			}
//
///*			if (bad_prefix_found == true) {
//				break;
//			} */
//		}
//
//		if (IIS_path_segment_found == true) {
//			std::cout<<"Path No: "<<pathNo<<" --The current path contains IIS path segment : skipping the path."<<endl;
//			infeasiblePaths.push_back(*pitr); //pushing the path in the infeasible path set
//			continue;
//		}
//
///*		if (bad_prefix_found == true) {
//			std::cout<<"Path No: "<<pathNo<<" --The current path contains BAD_PREFIX : skipping the path."<<endl;
//			continue;
//		} */
//
//		/****BAD_PREFIX END****/
//
//
//		transition::ptr edge = rply.d_Replay(ha_ptr1, *pitr, Edges); //############ Calling d_Replay ##############
//
//		if (edge != nullptr){	//Check if the edge is already detected
//			infeasiblePaths.push_back(*pitr); //pushing the path in the infeasible path set
//			bool edgeCheck = false;
//			for(auto ePtr = Edges.begin(); ePtr != Edges.end(); ePtr++){
//				if (edge == *ePtr){
//					edgeCheck = true;
//					break;
//				}
//			}
//			if (edgeCheck != true){
//				Edges.push_back(edge);
//			}
//
//		}
//
//		/****** Generate the xml & cfg for a linear path automata from human model ******/
//		//std::array<std::string,2> fileXmlCfg; //for receiving generated xml and cfg file
//		string str;	//for receiving path to generated xml and cfg file
//		bool path_feasible;
//		if (edge == nullptr){
//			paths_replayed_in_agent_model++;
//			fstream file, file1;
//			string file_path = "/home/asif/eclipse-workspace/XSpeed-plan/testcases/My_nav/unreachable/";
//			//string model ="nav_model_human_3";
//			//string model ="warehouse_automation_human1";
//			//string model ="warehouse_automation_human1_prob02";
//			//string model ="warehouse_automation_human1_prob03";
//			string model ="water_level_monitor_human";
//			string xml_file = file_path + model + ".xml";
//			string cfg_file = file_path + model + ".cfg";
//			file.open(xml_file, ios::out | ios::in);
//			std::cout<<"Opening "<<xml_file<<endl;
//			//generate the xml file for a linear path automata from human model
//			if (file.is_open()){
//				str = rply.generateXml(file, *pitr, file_path, model, pathNo);
//				//std::cout<<fileXmlCfg[0]<<endl;
//				//std::cout<<fileXmlCfg[1]<<endl;
//			}
//			file.close();
//
//			//generate the cfg file for a linear path automata from human model
//			file1.open(cfg_file, ios::out | ios::in);
//			std::cout<<"Opening "<<cfg_file<<endl;
//			bool agent_model = false;
//			if (file1.is_open()){
//				rply.generatecfg(file1, *pitr, file_path, model, pathNo, agent_model);
//			}
//			file1.close();
//
//
//			/****** Calling cH_Replay() (continuous replay) to check feasibility of a path in the human model ******/
//			path_feasible = rply.cH_Replay(str);
//			/*******************************************************/
//
//
//			if (path_feasible == false){
//				infeasiblePaths.push_back(*pitr); //storing infeasible paths
//			}
//
//			/****** Generate the xml & cfg for a linear path automata from the Agent model ******/
//			string str1;
//			std::pair<vector<int>, vector<int> > IIS_path_segment;
//			if (path_feasible == true){
//				feasible_paths_in_human_model++;
//				fstream file, file1;
//				//string file_path = "/home/asif/XSpeed-plan/testcases/My_nav/unreachable/";
//				string file_path = "/home/asif/eclipse-workspace/XSpeed-plan/testcases/My_nav/unreachable/";
//				//string model = "nav_model_agent_3";
//				//string model = "warehouse_automation_agent1";
//				//string model = "warehouse_automation_agent1_prob02";
//				//string model = "warehouse_automation_agent1_prob03";
//				string model = "water_level_monitor_agent";
//				string xml_file = file_path + model + ".xml";
//				string cfg_file = file_path + model + ".cfg";
//				file.open(xml_file, ios::out | ios::in);
//				std::cout<<"Opening "<<xml_file<<endl;
//				if (file.is_open()){
//					str1 = rply.generateXml(file, *pitr, file_path, model, pathNo);
//				}
//				file.close();
//
//				file1.open(cfg_file, ios::out | ios::in);
//				std::cout<<"Opening "<<cfg_file<<endl;
//				bool agent_model = true;
//				if (file1.is_open()){
//					rply.generatecfg(file1, *pitr, file_path, model, pathNo, agent_model);
//				}
//
//				/****** Calling cA_Replay() (continuous replay) to find IIS path-segments of a path in the agent model ******/
//				IIS_path_segment = rply.cA_Replay(str1);
//
//				infeasiblePaths.push_back(*pitr); //pushing the path in the infeasible path set
//
//				IIS_pathSegments.push_back(IIS_path_segment);	//storing all IIS path segments
//				/******************************************************************/
//
//
//			}
//			/*********/
//		}
//
//
//	}
	/****** Print the found edges that are not present in the agent model ******/
/*	std::cout<<"Number of edges not present in the agent model: "<<Edges.size()<<endl;
	//std::list<transition::ptr>::const_iterator ePtr;
	for(auto ePtr = Edges.begin(); ePtr != Edges.end(); ePtr++){
		std::cout<<(*ePtr)->getTransitionId()<<"  ";
	}
	std::cout<<""<<endl;
*/
	std::cout<<"Number of paths in the human model: "<<paths.size()<<endl;
//	std::cout<<"Number of paths in the agent model: "<<paths1.size()<<endl;
//	std::cout<<"Number of paths replayed in the agent model: "<<paths_replayed_in_agent_model<<endl;
//	std::cout<<"Number of feasible paths in the human model: "<<feasible_paths_in_human_model<<endl;
//	std::cout<<"Number of invalid edges detected: "<<Edges.size()<<endl;
//	std::cout<<"Number of infeasible paths: "<<infeasiblePaths.size()<<endl;
//	std::cout<<"Number of IIS path segments:"<<IIS_pathSegments.size()<<endl;


	std::cout<<"Test Today..."<<endl;
//	math::matrix<int> mat = ha_ptr->getGraph();
	math::matrix<transition::ptr> mat = ha_ptr->getGraph();
	//typedef boost::adjacency_matrix<boost::directedS, int> BoostAdjacencyMatrix;

	auto start = std::chrono::high_resolution_clock::now();
	ha_ptr->printGraph(ha_ptr->bglGraph(mat));

	ha_ptr->bglDFSpaths(ha_ptr->bglGraph(mat), 3, 19, 10);

	auto stop = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

	std::cout << "bglDFSpaths execution time: " << duration.count() << " milliseconds" << std::endl;

	/*auto start1 = std::chrono::high_resolution_clock::now();
//		ha_ptr->printGraph(ha_ptr->bglGraph(mat));

		ha_ptr->customDFSPaths( 6, 17, 10);

		auto stop1 = std::chrono::high_resolution_clock::now();

		auto duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(stop1 - start1);

		std::cout << "bglDFSpaths execution time: " << duration1.count() << " milliseconds" << std::endl;

*/
}

void themeSelector::select(){

	// ----Selects trajectory simulation
	if (boost::algorithm::iequals(userOps.getEngine(),"simu")==true) {
		selectSim();
		return;
	}

	// Select reachability with CE generation
	if(boost::algorithm::iequals(userOps.getEngine(),"reach")==true){
		selectReach();
		return;
	}

	//select falsification
	if(boost::algorithm::iequals(userOps.getEngine(),"fal")==true){
		selectFal();
		return;
	}


	//select path replay
	if(boost::algorithm::iequals(userOps.getEngine(),"replay")==true){
		selectReplay();
		return;
	}

	// ----Section for Running Exp-Graph. This code is put only for experimental task.
	int	 runExpGraph_WoFC = 0;	// To run Exp-Graph Algorithm, that is, Explore the Graph,
	//we should assign a valid loc-id in the forbidden set (and not -1, unlike FC algo)
	if (runExpGraph_WoFC) {
		bool found_CE = runWoFC_counter_example(*ha_ptr, init, forbidden[0], userOps);

		if (found_CE) {
			string cmdStr1;
			//cmdStr1.append("graph -TX -BC -W 0.008 out.txt -s -m 3 bad_poly -s -m 2 init_poly");
			//cmdStr1.append("graph -TX -BC -W 0.008 out.txt");
			//system(cmdStr1.c_str());
		}
		return;
	}
	//End of Section Exp-Graph.

}
themeSelector::~themeSelector() {

	// TODO Auto-generated destructor stub
}

