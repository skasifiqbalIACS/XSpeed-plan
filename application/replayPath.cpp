/*
 * replayPath.cpp
 *
 *  Created on: 27-Sep-2022
 *      Author: Sajid Sarwar
 */

#include <application/replayPath.h>
#include <fstream>
#include <boost/tokenizer.hpp>
#include <stdio.h>
#include <algorithm>
#include <string>
#include <regex>
#include <vector>


using namespace boost;

replayPath::~replayPath() {
	// TODO Auto-generated destructor stub
}

replayPath::replayPath(const hybrid_automata::ptr haPtr, const hybrid_automata::ptr haPtr2,  const std::list<initial_state::ptr>& init,
		const forbidden_states& forbidden, const userOptions& user_ops) : ha_ptr(haPtr), ha_ptr2(haPtr2), init(init), forbidden_s(forbidden),
				user_ops(user_ops){
	this->k = user_ops.get_bfs_level();
}

replayPath::replayPath(const hybrid_automata::ptr haPtr, const std::list<initial_state::ptr>& init,
		const forbidden_states& forbidden, const userOptions& user_ops) : ha_ptr(haPtr), init(init), forbidden_s(forbidden),
				user_ops(user_ops){
	this->k = user_ops.get_bfs_level();
}


std::list<structuralPath::ptr> replayPath::getStructuralPath(const hybrid_automata::ptr haPtrx, unsigned int src, unsigned int forbidden_loc_id, unsigned int depth){

	/*
	std::list<structuralPath::ptr> path_list; // It is empty here.

	unsigned int srcLoc = getInitialLocation()->getLocId();
	unsigned int destLoc = forbidden_loc_id;
	path_list = findAllPaths(srcLoc, destLoc, depth);

	return path_list;
	*/

	//std::list<structuralPath::ptr> path_list = ha_ptr->getStructuralPaths(forbidden_loc_id, depth);
	//std::list<structuralPath::ptr> path_list = ha_ptr->findAllPaths(src, forbidden_loc_id, depth);
	std::list<structuralPath::ptr> path_list = haPtrx->findAllPaths(src, forbidden_loc_id, depth);
	return path_list;
}

/* structuralPath::ptr replayPath::getaStructuralPath(const hybrid_automata::ptr haPtrx, ){

} */

transition::ptr replayPath::d_Replay(const hybrid_automata::ptr haPtrx, structuralPath::ptr path, std::list<transition::ptr> Edges){
	transition::ptr edge;
	//bool t_matched = false;
	std::list<location::const_ptr> path_locations = path->get_path_locations();
	/* std::cout<<"The path being replayed: ";
	for (auto it = path_locations.begin(); it != path_locations.end(); it++){
		std::cout<<(*it)->getLocId()<<" ";
	}
	std::cout<<""<<endl; */
    std::list<transition::ptr> path_transitions = path->get_path_transitions();
	/*for (auto it = path_locations.begin(); it != path_locations.end(); it++){
		int locId = (*it)->getLocId();
	} */
    //std::list<location::const_ptr>::const_iterator lptr = path_locations.begin();
    //std::list<transition::ptr>::iterator tptr = path_transitions.begin();
	for (unsigned int i = 0; i < path_locations.size(); i++){
		if (i != (path_locations.size()-1)){
			std::list<location::const_ptr>::const_iterator lptr = path_locations.begin();
			std::list<transition::ptr>::iterator tptr = path_transitions.begin();
			std::advance(lptr, i);
			int locId = (*lptr)->getLocId();
			std::cout<<"Location Id: "<<locId<<" iteration: "<<i<<endl;
			std::advance(tptr, i);
			int transId = (*tptr)->getTransitionId();

			/* If the current edge is found in the Edges list. */
			bool edgeFound = false;
			for (auto Edges_itr = Edges.begin(); Edges_itr != Edges.end(); Edges_itr++){
				if (transId == (*Edges_itr)->getTransitionId()){
					edgeFound = true;
					break;
				}
			}
			if (edgeFound == true){
				edge = *tptr;
				std::cout<<"The edge is not present in the agent model. The path is not checked."<<endl;
				break; //If edge is found in the Edges list, skip the path.
			}
			/***********************/

			location::ptr locPtr = haPtrx->getLocationNew(locId);
			const std::list<transition::ptr>& out_trans = locPtr->getOutGoingTransitions();
			std::cout<<out_trans.size()<<endl;
			if(out_trans.size() > 0){
				std::list<transition::ptr>::const_iterator itr;
					for (itr=out_trans.begin(); itr !=out_trans.end(); itr++){
						int transID = (*itr)->getTransitionId();
						if (transID != transId){
							continue;
						}else {
							std::cout<<"Transition matched"<<endl;
							//bool t_matched = true;
							break;}
					}
			}else {
				edge = *tptr;
				std::cout<<"The edge is not present in the agent model. Transition Id is: "<<transId<<endl;
				break;
			}

			/* if (t_matched == true){
				t_matched = false;
				//edge = *tptr;
				//std::cout<<"Path: "<<i<<" The edge is not present in the agent model. Transition Id is: "<<transId<<endl;
				//break;
			} */
		}else {
			std::cout<<"Path replayed in the agent model"<<endl;
			return nullptr;
		}

	}
	//std::cout<<edge->getTransitionId()<<" ";
	return edge;
}

void replayPath::modifyAgentHA(int locId, int transId, int newtransId){
	location::ptr locPtr = ha_ptr->getLocationNew(locId);
	std::cout<<locPtr->getLocId()<<endl;
	std::list<transition::ptr>::const_iterator it;
	for (it = locPtr->getOutGoingTransitions().begin(); it != locPtr->getOutGoingTransitions().end(); it++){
			int transID = (*it)->getTransitionId();
		    std::cout<<transID<<endl;
		    //polytope::const_ptr guardPtr = (*it)->getGuard();
            //guardPtr->printPoly();
			if (transID==transId){
				(*it)->setTransitionId(newtransId);
			}
		}
	//locPtr->removeOutGoingTransition(transId);
	//ha_ptr2->getLocation(locId)->removeOutGoingTransition();
}

string replayPath::generateXml(fstream& file, structuralPath::ptr path, string file_path, string model, int pathNo){
	//fstream file;
	string line;
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	//typedef boost::tokenizer<boost::escaped_list_separator<char> > tokenizer;

	//std::cout<<"opening nav_model_human_2.xml"<<endl;
	//file.open("/home/iacs1/eclipse-workspace/XSpeed/testcases/My_nav/unreachable/nav_model_human_2.xml", ios::out | ios::in);
	//file.open("nav_model_human_2.xml", ios::out | ios::in);

	//string str = "/home/iacs1/eclipse-workspace/XSpeed/testcases/My_nav/unreachable/nav_model_human_path" + to_string(pathNo); //path of xml & cfg file
	string str = file_path + "results/" + model + "_path" + to_string(pathNo); //path of generated xml & cfg file
	string str_xml = str + ".xml"; //output xml file

	//std::array<std::string,2> fileXmlCfg{str, str_cfg};	//for returning xml and cfg file
	fstream fileout;
	//fileout.open("/home/iacs1/eclipse-workspace/XSpeed/testcases/My_nav/unreachable/nav_model_human_2_path.txt", ios::out);
	fileout.open(str_xml, ios::out);

	//std::vector<std::string> loc_inv;	//storing location invariant
	//std::vector<std::string> loc_flow;	//storing location flow
	std::array<std::string,100> loc_inv;	//storing location invariant
	std::array<std::string,100> loc_flow;	//storing location flow
	std::array<std::string,100> trans_guard;	//storing transition guard
	std::array<std::string,100>trans_assignment; //storing transition assignment

	//if (file.is_open()) {
		while (getline(file, line)){
			// if empty line, then continue
			if(line.compare("")==0)
				continue;

			//char_separator<char> sep(" <>=:");
			boost::char_separator<char> sep(" <>=:\"");
			//boost::escaped_list_separator<char> sep( ' ', '\"', '\=' );
			//tokenizer<char_separator<char> > tokens(line,sep);
			//boost::tokenizer<char_separator<char> > tokens(line,sep);
			tokenizer tokens(line,sep);
			//tokenizer<escaped_list_separator<char> > tokens (line, sep);
			//boost::tokenizer<char_separator<char> >::iterator tok_iter;
			tokenizer::iterator tok_iter;
			int tokNo = 0;
			for(tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter){
				tokNo++;
			    //std::cout << "<" << *tok_iter << "> ";
			    //fileout<<"<" << *tok_iter << "> ";
			    if(*tok_iter == "?xml" || *tok_iter == "sspaceex" || *tok_iter == "version"
			    	|| *tok_iter == "component"){
			    	fileout<<line<<endl;
			    	break;
			    }

			    else if (*tok_iter == "param"){
					//tok_iter = tok_iter + 2;
			    	std::advance(tok_iter, 2);
					regex strExpr("(e)(.*)");
					if (!regex_match(*tok_iter, strExpr)){
						fileout<<line<<endl;
						break;
					}
					else {
						break;
					}
				}

			    else if(*tok_iter == "location" ){
			    	std::advance(tok_iter, 2);
			    	int locID = stoi(*tok_iter);
			    	//tok_iter = tok_iter + 2;
			    	getline(file, line); //line++;
			    	//std::cout<<line<<endl;
					boost::char_separator<char> sep3(" <>=:\"");
			    	tokenizer tokens3(line, sep3);
					tokenizer::iterator tok_iter3 = tokens3.begin();
					if (*tok_iter3 == "invariant"){
						//loc_inv.push_back(line);
						loc_inv[locID] = line;
				    	getline(file, line); //line++;
				    	//loc_flow.push_back(line);
				    	loc_flow[locID] = line;
				    	break;
					}
					else {
				    	//loc_flow.push_back(line);
						loc_flow[locID] = line;
				    	break;
					}


			    }
			    else if(*tok_iter == "transition"){
			    	getline(file, line); //line++;
					boost::char_separator<char> sep1(" <>=:\"e");
			    	tokenizer tokens1(line, sep1);
					tokenizer::iterator tok_iter1;
					for(tok_iter1 = tokens1.begin(); tok_iter1 != tokens1.end(); ++tok_iter1){
						if (*tok_iter1 == "lab"){
							//tok_iter1 = tok_iter1 + 2;
							std::advance(tok_iter1, 2);
							int edgeId = stoi(*tok_iter1);
							getline(file, line); //line++;
							boost::char_separator<char> sep2(" <>=:\"");
					    	tokenizer tokens2(line, sep2);
							tokenizer::iterator tok_iter2 = tokens2.begin();
							if(*tok_iter2 == "guard"){
								trans_guard[edgeId] = line;
								getline(file, line);
								boost::char_separator<char> sep4(" <>=:\"");
						    	tokenizer tokens4(line, sep4);
								tokenizer::iterator tok_iter4 = tokens4.begin();
								if (*tok_iter4 == "assignment"){
									trans_assignment[edgeId] = line;
								}
								break;
							}
							else if (*tok_iter2 == "assignment"){
								trans_assignment[edgeId] = line;
								break;
							}


						}
					}
					break;
			    }
			    else {
			    	break;
			    }

			}
			//std::cout<<" Number of tokens: "<<tokNo;
			//fileout<<" Number of tokens: "<<tokNo;
			//std::cout << "\n";
			//fileout<< "\n";
		}

	//}
	//file.close();

	/* setting transitions in the xml */
	std::list<transition::ptr> path_transitions = path->get_path_transitions(); //list of transitions
	int transNo = 0;	//number of transitions in the path
	for (auto it = path_transitions.begin(); it != path_transitions.end(); it++){
		transNo++;
		int transId = (*it)->getTransitionId();
		string str1 = "e" + to_string(transId) + "00" + to_string(transNo);
		fileout<<"    <param name=\""<<str1<<"\" type=\"label\" local=\"false\" />"<<endl;
	}

	/* setting locations in the xml */
	std::list<location::const_ptr> path_locations = path->get_path_locations(); //list of locations
	int locsNo = 0;		//number of locations in the path
	for(auto it = path_locations.begin(); it !=path_locations.end(); it++){
		locsNo++;
		int locId = (*it)->getLocId();
		string str2 = to_string(locId) + "00" + to_string(locsNo);
		fileout<<"    <location id=\""<<str2<<"\" name=\"loc"<<str2<<"\">"<<endl;
		fileout<<loc_inv[locId]<<endl;
		fileout<<loc_flow[locId]<<endl;
		fileout<<"    </location>"<<endl;
	}

	/* setting transitions in the xml */
	//std::list<transition::ptr> path_transitions = path->get_path_transitions(); //list of transitions
	transNo = 0;	//number of transitions in the path
	locsNo = 1;		//number of locations in the path
	std::list<location::const_ptr>::iterator locIter = path_locations.begin();
	for (auto it = path_transitions.begin(); it != path_transitions.end(); it++){
		transNo++;
		int transId = (*it)->getTransitionId();
		string str1 = "e" + to_string(transId) + "00" + to_string(transNo);
		int source = (*locIter)->getLocId();
		string str2 = to_string(source) + "00" + to_string(locsNo);
		std::advance(locIter, 1);
		int target = (*locIter)->getLocId();
		locsNo++;
		string str3 = to_string(target) + "00" + to_string(locsNo);
		fileout<<"    <transition source=\""<<str2<<"\" target=\""<<str3<<"\">"<<endl;
		fileout<<"      <label>"<<str1<<"</label>"<<endl;
		fileout<<trans_guard[transId]<<endl;
		fileout<<trans_assignment[transId]<<endl;
		fileout<<"    </transition>"<<endl;
	}

	fileout<<"  </component>"<<endl;
	fileout<<"</sspaceex>"<<endl;

	fileout.close();

	return str; //returning path to xml & cfg file
}

void replayPath::generatecfg(fstream& file1, structuralPath::ptr path, string file_path, string model, int pathNo, bool agent_model){
	string line;
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

	string str = file_path + "results/" + model + "_path" + to_string(pathNo); //path of generated cfg file
	string str_cfg = str + ".cfg"; //output cfg file

	fstream fileout;
	fileout.open(str_cfg, ios::out);

	std::list<location::const_ptr> path_locations = path->get_path_locations(); //list of locations
	std::list<location::const_ptr>::iterator locIter1 = path_locations.begin();
	int strt_loc = (*locIter1)->getLocId();
	std::advance(locIter1, (path_locations.size() - 1));
	int end_loc = (*locIter1)->getLocId();

	while (getline(file1, line)){

		if(line.compare("")==0)			// if empty line, then continue
			continue;

		boost::char_separator<char> sep(" <>=:\"");
		tokenizer tokens(line,sep);
		tokenizer::iterator tok_iter;

		for(tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter){

			//std::cout<<"<"<<*tok_iter<<"> ";
		    if (*tok_iter == "initially"){
		    	if (agent_model){
		    		//warehouse automation agent:
		    		  //prob01:
		    		    //fileout<<"initially = \"x1==0.5 & x2==1.5 & c==1 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		    //fileout<<"initially = \"x1==0.5 & x2==1.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		  //prob02
		    		    //fileout<<"initially = \"x1==0.5 & x2==3.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		  //prob03
		    		    //fileout<<"initially = \"x1==0.5 & x2==3.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		//water level monitor agent:
		    		fileout<<"initially = \"x1==0 & x2==0 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    	}
		    	else {
		    		//warehouse automation human:
		    		  //prob01:
		    		    //fileout<<"initially = \"x1==0.5 & x2==1.5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		    //fileout<<"initially = \"x1==0.5 & x2==1.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		  //prob02:
		    		    //fileout<<"initially = \"x1==0.5 & x2==3.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		  //prob03
		    		    //fileout<<"initially = \"x1==0.5 & x2==3.5 & c==5 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    		//water level monitor human:
		    		fileout<<"initially = \"x1==0 & x2==0 & loc()==loc"<<strt_loc<<"001\""<<endl;
		    	}
		    	break;
		    }
		    else if (*tok_iter == "forbidden"){
		    	fileout<<"forbidden = \"loc()==loc"<<end_loc<<"00"<<path_locations.size()<<"\""<<endl;
		    	break;
		    }
		    else {
		    	fileout<<line<<endl;
		    	break;
		    }
		}
		//std::cout<<""<<endl;
	}

	/****** Writing cfg file ******/
	//fstream filein_cfg, fileout_cfg;
/*	fstream fileout_cfg;
	//string cfg_path = file_path + ".cfg";
	//filein_cfg.open(cfg_path, ios::in);
	string str_cfg = str + ".cfg"; //output cfg file
	fileout_cfg.open(str_cfg, ios::out); */

	/*if (filein_cfg.open){
		// if empty line, then continue
		if(line.compare("")==0)
			continue;
		else
			fileout_cfg<<line<<endl;
	} */

/*	fileout_cfg<<"# analysis options"<<endl;
	fileout_cfg<<"system = \"nav_model_human\""<<endl;
	std::list<location::const_ptr>::iterator locIter1 = path_locations.begin();
	int strt_loc = (*locIter1)->getLocId();
	//fileout_cfg<<"initially = \"x1==0.5 & x2==1.5 & z>=-1 & z<=1 & loc()==loc"<<strt_loc<<"001\""<<endl;
	fileout_cfg<<"initially = \"x1==0.5 & x2==1.5 & loc()==loc"<<strt_loc<<"001\""<<endl;
	//int path_size = path_locations.size() - 1;
	std::advance(locIter1, (path_locations.size() - 1));
	//locIter1 = path_locations.end();
	int end_loc = (*locIter1)->getLocId();
	fileout_cfg<<"forbidden=\"loc()==loc"<<end_loc<<"00"<<path_locations.size()<<"\""<<endl;
	fileout_cfg<<"iter-max = 50"<<endl;
	fileout_cfg<<"rel-err = 1.0e-12"<<endl;
	fileout_cfg<<"abs-err = 1.0e-13"<<endl; */
	/************************/
	fileout.close();
}






bool replayPath::cH_Replay(string str){
/*	string cmd_str = "runlim -s 4096 -t 1000 -r 1000 -o" + " " + "results/My_nav/unreachable/nav_model_3_9_BACH.runlim"
			+ " " + "./bach -t -p -v2 -solver cmsat -semantics mixed BACH :" + " " +
			"benchmarks/My_nav/unreachable/nav_model_3.xml" + " " + "benchmarks/My_nav/unreachable/nav_model_3.cfg"
			+ " 9 >" + " " + "results/My_nav/unreachable/log/nav_model_3_9_BACH.log";
*/
/*	string cmd_str = "runlim -s 4096 -t 1000 -r 1000 -o " + str + ".runlim"
			+ " " + "/home/iacs1/vmcai22BACH/bach -t -p -v2 -solver cmsat -semantics mixed" + " " +
			str + ".xml" + " " + str + ".cfg"
			+ " 10 >" + " " + str + "BACH.log"; */
	string cmd_str = "runlim -s 4096 -t 1000 -r 1000 -o " + str + ".runlim"
			+ " " + "/home/iacs1/vmcai22BACH/bach -t -p -v2 -solver cmsat -semantics mixed" + " " +
			str + ".xml" + " " + str + ".cfg"
			+ " 20 >" + " " + str + "BACH.log";
	system(cmd_str.c_str()); //calling BACH

	//vector<int> locs;	//store loc_ids of a
	//vector<int> trans;
	//std::pair<vector<int>, vector<int> > path_segment;

	bool path_feasible = false;	//returns feasibility of a path

	fstream file;
	string line;
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	file.open(str + "BACH.log", ios::out | ios::in);
	std::cout<<"opening " + str + "BACH.log"<<endl;
	if (file.is_open()){
		while (getline(file, line)){
			// if empty line, then continue
			if(line.compare("")==0)
				continue;

			boost::char_separator<char> sep(" $()[]-><=\t\"");
			tokenizer tokens(line,sep);
			tokenizer::iterator tok_iter;
			int tokNo = 0;
			tok_iter = tokens.begin();
/*			if (*tok_iter == "nav_model_human:"){
				//std::cout<<"Test"<<endl;
				//std::cout << "<" << *tok_iter << "> ";
				while (tok_iter != tokens.end()){
					tokNo++;
					std::cout << "<" << *tok_iter << "> ";
					regex strExpr("(loc)(.*)");
					regex strExpr1("(e)(.*)");
					if (regex_match(*tok_iter, strExpr)){
						string loc_name = *tok_iter;
						string loc_id_str = "";
						char* char_array = new char[loc_name.length() + 1];
						strcpy(char_array, loc_name.c_str());
						//std::cout<<"loc id :"<<char_array[3]<<endl;
						if (char_array[3] != '0'){
							if (char_array[4] != '0'){
								if (char_array[5] != '0'){
									loc_id_str = loc_id_str + char_array[3] + char_array[4] + char_array[5];
								}
								else loc_id_str = loc_id_str + char_array[3] + char_array[4];
							}
							else loc_id_str = loc_id_str + char_array[3];
						}
						std::cout<<loc_id_str<<endl;
						delete[] char_array;
						int loc_id = stoi(loc_id_str);
						//location::const_ptr locPtr = haPtrx->getLocation(loc_id);
						locs.push_back(loc_id);
						std:cout<<"Loc Id: "<<loc_id<<endl;
					}
					else if (regex_match(*tok_iter, strExpr1)){
						string trans_name = *tok_iter;
						string trans_id_str = "";
						char* char_array1 = new char[trans_name.length()+1];
						strcpy(char_array1, trans_name.c_str());
						if (char_array1[1] != '0'){
							if (char_array1[2] != '0'){
								if (char_array1[3] != '0'){
									trans_id_str = trans_id_str + char_array1[1] + char_array1[2] + char_array1[3];
								}
								else trans_id_str = trans_id_str + char_array1[1] + char_array1[2];
							}
							else trans_id_str = trans_id_str + char_array1[1];
						}
						std::cout<<trans_id_str<<endl;
						delete[] char_array1;
						int trans_id = stoi(trans_id_str);
						trans.push_back(trans_id);
						std::cout<<"Trans Id: "<<trans_id<<endl;
					}
					std::advance(tok_iter,1);
				}
				std::cout<<" Number of tokens: "<<tokNo;
				std::cout<<"\n";
				path_segment.first = locs;
				path_segment.second = trans;
			}


			if (*tok_iter == "unsat"){
				//pathSegments.push_back(path_segment);
				path_feasible = false;
				break;
			} */
			if (*tok_iter == "sat"){
				path_feasible = true;
				break;
			}
			else
				continue;

		}
	}
	file.close();
	return path_feasible;

}

std::pair<vector<int>, vector<int> > replayPath::cA_Replay(string str){
/*	string cmd_str = "runlim -s 4096 -t 1000 -r 1000 -o " + str + ".runlim"
			+ " " + "/home/iacs1/vmcai22BACH/bach -t -p -v2 -solver cmsat -semantics mixed" + " " +
			str + ".xml" + " " + str + ".cfg"
			+ " 10 >" + " " + str + "BACH.log"; */
	string cmd_str = "runlim -s 4096 -t 1000 -r 1000 -o " + str + ".runlim"
			+ " " + "/home/iacs1/vmcai22BACH/bach -t -p -v2 -solver cmsat -semantics mixed" + " " +
			str + ".xml" + " " + str + ".cfg"
			+ " 20 >" + " " + str + "BACH.log";
	system(cmd_str.c_str()); //calling BACH

	vector<int> locs;	//store loc_ids of an IIS path segment
	vector<int> trans;	//store trans_ids of an IIS path segment
	std::pair<vector<int>, vector<int> > IIS_path_segment;

	//bool path_feasible = false;	//returns feasibility of a path

	fstream file;
	string line;
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	file.open(str + "BACH.log", ios::out | ios::in);
	std::cout<<"opening " + str + "BACH.log"<<endl;
	if (file.is_open()){
		while (getline(file, line)){
			// if empty line, then continue
			if(line.compare("")==0)
				continue;

			boost::char_separator<char> sep(" $()[]-><=\t\"");
			tokenizer tokens(line,sep);
			tokenizer::iterator tok_iter;
			//int tokNo = 0;
			tok_iter = tokens.begin();
			if (*tok_iter == "IIS"){
				//std::cout<<"Test"<<endl;
				//std::cout << "<" << *tok_iter << "> ";
				getline(file, line);
				boost::char_separator<char> sep1(" $()[]-><=\t\"");
		    	tokenizer tokens1(line, sep1);
				tokenizer::iterator tok_iter1;
				tok_iter1 = tokens1.begin();
				while (tok_iter1 != tokens1.end()){
					//tokNo++;
					//std::cout << "<" << *tok_iter1 << "> ";
					regex strExpr("(loc)(.*)");
					regex strExpr1("(e)(.*)");
					if (regex_match(*tok_iter1, strExpr)){
						string loc_name = *tok_iter1;
						string loc_id_str = "";
						char* char_array = new char[loc_name.length() + 1];
						strcpy(char_array, loc_name.c_str());
						//std::cout<<"loc id :"<<char_array[3]<<endl;
						if (char_array[3] != '0'){
							if (char_array[4] != '0'){
								if (char_array[5] != '0'){
									loc_id_str = loc_id_str + char_array[3] + char_array[4] + char_array[5];
								}
								else loc_id_str = loc_id_str + char_array[3] + char_array[4];
							}
							else loc_id_str = loc_id_str + char_array[3];
						}
						//std::cout<<loc_id_str<<endl;
						delete[] char_array;
						int loc_id = stoi(loc_id_str);
						//std::cout<<"IIS loc id: "<<loc_id<<endl;
						//location::const_ptr locPtr = haPtrx->getLocation(loc_id);
						locs.push_back(loc_id);
						//std:cout<<"Loc Id: "<<loc_id<<endl;
					}
					else if (regex_match(*tok_iter1, strExpr1)){
						string trans_name = *tok_iter1;
						string trans_id_str = "";
						char* char_array1 = new char[trans_name.length()+1];
						strcpy(char_array1, trans_name.c_str());
						if (char_array1[1] != '0'){
							if (char_array1[2] != '0'){
								if (char_array1[3] != '0'){
									trans_id_str = trans_id_str + char_array1[1] + char_array1[2] + char_array1[3];
								}
								else trans_id_str = trans_id_str + char_array1[1] + char_array1[2];
							}
							else trans_id_str = trans_id_str + char_array1[1];
						}
						//std::cout<<trans_id_str<<endl;
						delete[] char_array1;
						int trans_id = stoi(trans_id_str);
						//std::cout<<"IIS trans id: "<<trans_id<<endl;
						trans.push_back(trans_id);
						//std::cout<<"Trans Id: "<<trans_id<<endl;
					}
					std::advance(tok_iter1,1);
				}
				//std::cout<<" Number of tokens: "<<tokNo;
				//std::cout<<"\n";
				IIS_path_segment.first = locs;
				IIS_path_segment.second = trans;
				break;
			}
			else continue;
		}
	}
	file.close();
	std::cout<<"IIS locs are: ";
	for (unsigned int i = 0; i < IIS_path_segment.first.size(); i++) {
		std::cout<<IIS_path_segment.first[i]<<" ";
	}
	std::cout<<"\n";
	std::cout<<"IIS trans are: ";
	for (unsigned int i = 0; i < IIS_path_segment.second.size(); i++) {
		std::cout<<IIS_path_segment.second[i]<<" ";
	}
	std::cout<<"\n";
	return IIS_path_segment;

}

