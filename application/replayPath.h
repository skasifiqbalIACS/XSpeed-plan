/*
 * replayPath.h
 *
 *  Created on: 27-Sep-2022
 *      Author: Sajid Sarwar
 */

#ifndef APPLICATION_REPLAYPATH_H_
#define APPLICATION_REPLAYPATH_H_

#include <core/hybridAutomata/hybridAutomata.h>
#include <core/symbolicStates/symbolicStates.h>
#include <core/symbolicStates/initialState.h>
//#include <core/continuous/approxModel/fbInterpol.h>
//#include <core/reachability/postCSequential.h>
#include <utilities/templatePolyhedra.h>
//#include <utilities/postAssignment.h>
#include <application/userOptions.h>
#include <boost/algorithm/string.hpp>
#include <io/ioUtility.h>
#include <vector>
#include <list>
//#include <z3++.h>
//#include <counterExample/abstractCE.h>
//#include <core/symbolicStates/initialState.h>
//#include <core/reachability/reachability.h>
#include "utilities/statistics.h"
#include "core/hybridAutomata/location.h"
#include <fstream>

/*
 * Replay of a path of Human's model HA_H is performed on the
 * discrete structure of Agent's model HA_A.
 * If the path is not replayed on HA_A, we remove the first
 * transition (edge) that is not found in HA_A from HA_H.
 * If the path is replayed in discrete structure, we checks
 * the feasibility of the path with continuous dynamics.
 *
 */

typedef std::vector<unsigned int> path;
typedef std::vector<unsigned int> Run;
typedef std::pair<int, polytope::ptr> forbidden; //(locId,Polytope)
typedef std::vector<forbidden> forbidden_states; // vector of forbidden symb states.


class replayPath {

		const hybrid_automata::ptr ha_ptr;
		//const hybrid_automata::ptr ha_ptr1;
		const hybrid_automata::ptr ha_ptr2;
		const std::list<initial_state::ptr>& init;
		const forbidden_states& forbidden_s;
		const userOptions& user_ops;

		//reachability reach_for_CE;

		//z3::context c;
		//z3::expr ha_encoding;
		//z3::solver sol;
		//z3::context cc;
		//z3::solver s1; //for second solver.

		unsigned int k; // The bound on path-length, i.e., the number of edges.

		/* uses this private member function to initialize the ha_encoding for the given
		 * forbidden location id
		 */
		//void init_solver(unsigned int k1);

public:
		/* To be used for replayPath when a set of ha locations together with assoc. polytopes are forbidden
		 *
		 *
		 */
		replayPath(const hybrid_automata::ptr haPtr, const hybrid_automata::ptr haPtr1, const std::list<initial_state::ptr>& init, const forbidden_states& forbidden,
			 const userOptions& user_ops);

		replayPath(const hybrid_automata::ptr haPtr, const std::list<initial_state::ptr>& init, const forbidden_states& forbidden,
					 const userOptions& user_ops);

		virtual ~replayPath();


		/**
		 * Returns a structural path in the HA starting from the initial location and ending at the
		 * forbidden_location (passed as a parameter). The path of length at-most depth is considered.
		 *
		 */
		std::list<structuralPath::ptr> getStructuralPath(const hybrid_automata::ptr haPtrx, unsigned int src, unsigned int forbidden_loc_id, unsigned int depth);

		/**
		 * Find a structural path in the HA starting from the initial location and ending at the
		 * forbidden_location (passed as a parameter). The path of length at-most depth is considered.
		 */
		//structuralPath::ptr findAPath(int src, int dst, int depthBound);


		/**
		 * Replay a path of Human model to the Agent model, if the path is not replayable
		 * returns the edge (transition) of the Human model which is not present in the Agent model.
		 */
		transition::ptr d_Replay(const hybrid_automata::ptr haPtrx, structuralPath::ptr path, std::list<transition::ptr> Edges);


		/**
		 * Modify Agent HA object
		 */
		void modifyAgentHA(int locId, int transId, int newtransId);

		/*
		 * Generates the xml file for the linear path automata.
		 */
		string generateXml(fstream& file, structuralPath::ptr path, string file_path, string model, int pathNo);

		/*
		 * Generates the cfg file for the linear path automata.
		 */
		void generatecfg(fstream& file1, structuralPath::ptr path, string file_path, string model, int pathNo, bool agent_model);

		/*
		 * Continuous replay a path for feasibility analysis in the human model
		 */
		bool cH_Replay(string str);

		/*
		 * Continuous replay a path for feasibility analysis and
		 * finding IIS path-segments in the agent model
		 */
		std::pair<vector<int>, vector<int> > cA_Replay(string str);
};

#endif /* APPLICATION_REPLAYPATH_H_ */
